const {
  app,
  BrowserWindow
} = require('electron');

// Global reference of the window object.
let mainWindow;

// When Electron finish initialization, create window and load app index.html
app.on('ready', () => {
  mainWindow = new BrowserWindow({
    webPreferences: {
      nodeIntegration: false
    }
  });

  mainWindow.maximize();

  // Path to your index.html
  mainWindow.loadURL('file://' + __dirname + '/dist/index.html');
});
