//General
var gulp = require('gulp');
var replace = require('gulp-replace');
var rename = require('gulp-rename');
var gutil = require('gulp-util');
var es = require('event-stream');
var prettydata = require('gulp-pretty-data');
var gulpif = require('gulp-if');
var lazypipe = require('lazypipe');
var ui5preload = require('gulp-ui5-preload');
var plumber = require('gulp-plumber');

//Scripts and tests
var htmlhint = require('gulp-htmlhint');
var jsonlint = require("gulp-jsonlint");
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babel = require("gulp-babel");


//Styles
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var sourcemaps = require('gulp-sourcemaps');

//Toolsnpm inst
var browserSync = require('browser-sync');
var reload = browserSync.reload;
var inquirer = require('inquirer');

//Runtime variables
var CONFIG = {
  "app_name": "Pack",
  "app_theme": "sap_belize_plus",
  "app_resource": "https://sapui5.hana.ondemand.com/resources/sap-ui-core.js",
  "app_resource_production": "../resources/sap-ui-core.js",
  "production": !!gutil.env.production,
  "namespace": "app"
};

var onError = function(err) {
  gutil.log(err.toString());
  this.emit('end');
};

gulp.task('html', function() {
  return gulp.src('app/*.html')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(replace("{{app_name}}", CONFIG.app_name))
    .pipe(replace("{{app_theme}}", CONFIG.app_theme))
    .pipe(replace("{{app_resource}}", CONFIG.production ? CONFIG.app_resource_production : CONFIG.app_resource))
    .pipe(replace("{{namespace}}", CONFIG.namespace))
    .pipe(htmlhint())
    .pipe(htmlhint.reporter("htmlhint-stylish"))
    .pipe(gulp.dest('dist'))
})

gulp.task('png', function() {
  return gulp.src('app/**/**.png')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('json', function() {
  return gulp.src('app/**/**.json')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(replace("{{namespace}}", CONFIG.namespace))
    .pipe(jsonlint())
    .pipe(jsonlint.reporter(function(file) {
      gutil.log('File ' + file.path + ' is not a valid JSON file.');
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('js', function() {
  return gulp.src('app/**/**.js')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(replace("{{namespace}}", CONFIG.namespace))
    .pipe(babel())
    .pipe(jsonlint())
    .pipe(jsonlint.reporter(function(file) {
      gutil.log('File ' + file.path + ' is not a valid JSON file.');
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('xml', function() {
  return gulp.src('app/**/**.xml')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(replace("{{namespace}}", CONFIG.namespace))
    .pipe(jsonlint())
    .pipe(jsonlint.reporter(function(file) {
      gutil.log('File ' + file.path + ' is not a valid XML file.');
    }))
    .pipe(gulp.dest('dist'))
})

gulp.task('sass', function() {
  return gulp.src('app/scss/*.scss')
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(sourcemaps.init())
    .pipe(sass())
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(concat('style.css'))
    .pipe(gulp.dest('dist/css'))
    .pipe(browserSync.stream())
})

gulp.task('preload', function() {
  var jshintChannel = lazypipe()
    .pipe(babel)
    .pipe(uglify);
  var xmlChannel = lazypipe()
    .pipe(prettydata, {
      type: 'minify'
    });

  return gulp.src([
      'app/**/**.+(js|xml|json|properties|png)'
    ])
    .pipe(plumber({
      errorHandler: onError
    }))
    .pipe(replace("{{namespace}}", CONFIG.namespace))
    .pipe(gulpif('**/*.js', jshintChannel())) //only pass .js files
    .pipe(gulpif('**/*.xml', xmlChannel())) // only pass .xml files
    .pipe(ui5preload({
      base: 'app/',
      namespace: CONFIG.namespace
    }))
    .pipe(gulp.dest('dist'));
})


gulp.task('browserSync', function() {
  browserSync({
    server: {
      baseDir: 'dist'
    }
  });
})

gulp.task('watch', function() {
  gulp.watch('app/scss/*.scss', ['sass']);
  gulp.watch('app/*.html', ['html']).on('change', reload);
  gulp.watch('app/**/**.json', ['json']).on('change', reload);
  gulp.watch('app/**/**.js', ['js']).on('change', reload);
  gulp.watch('app/**/**.xml', ['xml']).on('change', reload);
  gulp.watch('app/**/**.+(js|xml|properties)', ['preload']).on('change', reload);
})

gulp.task('test', function(cb) {
  return cb;
})

gulp.task('build', ['html', 'js', 'json', 'xml', 'sass', 'png', 'preload'])

gulp.task('default', ['build', 'browserSync', 'watch'])
