sap.ui.define([
    "sap/ui/core/UIComponent",
    "sap/ui/model/resource/ResourceModel"
], function (UIComponent, ResourceModel) {
    "use strict";
    return UIComponent.extend("{{namespace}}.Component", {

        metadata: {
            rootView: "{{namespace}}.view.App",
            includes: ["css/style.css"],
            models: {
                sidebarNavigation: {
                    "type": "sap.ui.model.json.JSONModel",
                    "uri": "./view/sidebarNavigation.json"
                }
            }
        },

        init: function () {
            // call the init function of the parent
            UIComponent.prototype.init.apply(this, arguments);

            // Set i18n model
            let i18nModel = new ResourceModel({
                bundleName: "{{namespace}}.i18n.i18n"
            });

            jQuery.sap.log.setLevel(jQuery.sap.log.Level.INFO, "SALT");

            this.setModel(i18nModel, "i18n");
        }
    });
});
