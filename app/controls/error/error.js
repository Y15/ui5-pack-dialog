sap.ui.define([
    "sap/ui/base/Object"
], function (Object) {
    "use strict";

    let instance;
    let Error = Object.extend("{{namespace}}.ErrorHandler", {
        rootError: sap.ui.getCore().rootError,

        errorText: new sap.ui.model.json.JSONModel({
            errorText: []
        }),

        constructor: function () {
            this.rootError = sap.ui.getCore().rootError;

            this._initError();
        },

        // Use this for CallBacks
        showError: function (response) {
            let error = this._getErrorText(response);
            let that = this;

            if (typeof error === "string") {
                this._showErrorText(error);
            } else {
                jQuery(error).each(function (i, v) {
                    that._showErrorText(v);
                });
            }
        },

        //Use this for normal text/array
        raiseError: function (error) {
            let that = this;

            if (typeof error === "string") {
                this._showErrorText(error);
            } else {
                jQuery(error).each(function (i, v) {
                    that._showErrorText(v);
                });
            }
        },

        _showErrorText: function (errorText) {
            //Append error Text
            let oError = this._getErrorArray();
            oError.push(errorText);

            this.errorText.setProperty("/errorText", oError);

            //Show error
            this._errorPopUp.open();
        },

        //Returns Model Text
        _getErrorArray: function () {
            return this.errorText.getProperty("/errorText");
        },

        //4. NO HTML => reponde the string or default error
        _getErrorDefault: function (response) {
            if (response.length > 0) {
                return response;
            } else {
                let i18n = sap.ui.getCore().getModel("i18n").getResourceBundle();
                return i18n.getText("errorPopUp.default.error");
            }

        },

        //3. HTML
        _getErrorHtml: function (response) {
            let html = jQuery(response).find("td p"),
                error = [];

            html.each(function (i, v) {
                error.push(jQuery(v).text());
            });


            if (error.length < 1) {
                error = this._getErrorDefault(response);
            }

            return error;
        },

        //2. XML
        _getErrorXML: function (response) {
            try {
                let oError = (jQuery(jQuery.parseXML(response)));
                let msg = oError.find("message").text();

                if (msg.length > 0) {
                    return msg;
                } else {
                    return this._getErrorHtml(response);
                }

            } catch (e) {
                return this._getErrorHtml(response);
            }
        },

        //1. JSON
        _getErrorJSON: function (response) {
            try {
                let oError = JSON.parse(response);
                return oError.error.message.value;
            } catch (e) {
                return this._getErrorXML(response);
            }
        },

        //Get error Text from Event
        //Normally we would check the response header for the type => we do not got this info here!
        //Because of this reason, we need to check it trial and error.
        //1. Json, 2.XML, 3.HTML, 4. Return the Text
        _getErrorText: function (oError) {
            try {
                let oResponse;
                if (oError.responseText) {
                    oResponse = oError;
                } else {
                    oResponse = oError.getParameter("response");
                }

                let message;
                if (oResponse.responseText.length > 0) {
                    message = this._getErrorJSON(oResponse.responseText);
                } else {
                    message = this._getErrorJSON(oResponse.message);
                }

                return message;

            } catch (e) {
                return this._getErrorJSON(oError);
            }
        },

        //Init error(Only one instance!)
        _initError: function () {

            if (!this.rootError) {
                this._errorPopUp = sap.ui.xmlfragment("{{namespace}}.controls.error.popup", this);

                let sLocale = sap.ui.getCore().getConfiguration().getLanguage();
                let oBundle = new sap.ui.model.resource.ResourceModel({
                    bundleUrl: "{{namespace}}/controls/error/i18n.properties",
                    locale: sLocale
                });
                this._errorPopUp.setModel(oBundle, "i18n");

                jQuery.sap.includeStyleSheet("{{namespace}}/controls/error/error.css");

                this.rootError = this;
                sap.ui.getCore().rootError = this;

                this._setErrorSize();
            } else {
                this._errorPopUp = sap.ui.getCore().rootError._errorPopUp;
            }

            this._errorPopUp.setModel(this.errorText);

        },

        _determineNewErrorSizePX: function () {
            let errorPopUpHeight = this._errorPopUp.$().height();

            return {
                newFontSize: errorPopUpHeight * 0.95,
                newLineHeight: errorPopUpHeight * 0.8
            };
        },

        _setErrorSize: function () {

            this._errorPopUp.addEventDelegate({
                "onAfterRendering": function () {
                    this._resize();
                }
            }, this);

            this._setCallBackResizeErrorSize();
        },

        _setCallBackResizeErrorSize: function () {
            let that = this;
            jQuery(window).resize(function () {
                that._resize();
            });
        },

        _resize: function () {
            let newHeight = this._determineNewErrorSizePX();
            let oError = sap.ui.getCore().byId("errorDialogTopText");
            let that = this;

            if (newHeight.newFontSize === 0) {

                setTimeout(function () {
                    that._resize();
                }, 10);

            }

            oError.$().css({
                "font-size": newHeight.newFontSize + "px",
                "line-height": newHeight.newLineHeight + "px"
            });
        },

        onBtnOK: function () {
            this.errorText.setProperty("/errorText", []);
            this._errorPopUp.close();
        }

    });

    return {
        GetInstance: function () {
            if (!instance) {
                instance = new Error();
            }
            return instance;
        }
    };

});