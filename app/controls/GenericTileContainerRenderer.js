import Control from "sap/ui/core/Renderer";

export default class GenericTileContainerRenderer extends Control {
    metadata = {
        properties: {}
    }

    render(rm, control) {
        rm.write("<div");
        rm.writeControlData(control);
        rm.addStyle("height",control.getHeight());
        rm.addStyle("width",control.getWidth());
        rm.writeStyles();
        rm.addClass("dragg-dropp-container");
        rm.writeClasses();
        rm.write(" >");

        this.renderTiles(rm, control);
        rm.write("</div>");

    }

    renderTiles(rm, control) {
        let aTiles = control.getTiles();

        if (!aTiles.length) {
            return;
        }

        aTiles.forEach((v) => {
            rm.renderControl(v);
        });

    }

}
