import Control from "sap/ui/core/Control";

export default class GenericTileContainer extends Control {
    metadata = {
        properties: {
            width: {type: "sap.ui.core.CSSSize", group: "Dimension", defaultValue: '100%'},
            height: {type: "sap.ui.core.CSSSize", group: "Dimension", defaultValue: 'auto'},
        },
        defaultAggregation: "tiles",
        aggregations: {
            tiles: {type: "sap.m.GenericTile", multiple: true, singularName: "tile"}
        }
    }

}
