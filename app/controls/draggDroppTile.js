import Tile from "sap/m/GenericTile";
import jqueryUI from "{{namespace}}/controls/jquery-ui-1.12.1.custom/jquery-ui"

export default class DraggDroppTile extends Tile {
    metadata = {
        properties: {},
        events: {
            drop: {}
        }
    }

    renderer = "sap.m.GenericTileRenderer"

    onAfterRendering(el) {
        super.onAfterRendering();
        this._initInteractives(el.srcControl);
    }

    _getStatic() {
        return sap.ui.getCore().getStaticAreaRef();
    }

    _removeActiveClass(id) {
        let overlay = $("#" + id + " .sapMGTPressActive");
        overlay.removeClass("sapMGTPressActive");
    }

    _initInteractives(el) {
        let tile = el.$();

        tile.draggable({
            revert: true,
            helper: "clone",
            appendTo: this._getStatic(),
            zIndex: 999,
            start: () => {
                tile.css("visibility", "hidden");
            },
            stop: () => {
                el._removeActiveClass(el.getId());
                tile.css("visibility", "visible");
            }
        });

        tile.droppable({
            drop: function (event, ui) {
                this.fireDrop({srcTile: sap.ui.getCore().byId(ui.helper.context.id), destTile: this});
            }.bind(this)
        })
    }
}
