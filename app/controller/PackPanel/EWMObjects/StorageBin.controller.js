sap.ui.define([
  "./EWMObject"
], function(Controller) {
  "use strict";
  return Controller.extend("{{namespace}}.controller.PackPanel.EWMObjects.StorageBin", {

    setFilterStorageBin(storageBin) {
      console.log(storageBin);
    },

    onTilePress(e) {
      let key = e.getSource().data("key");
      this.navTo("handlingunit", key);
    }

  });
});
