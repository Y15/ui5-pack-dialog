sap.ui.define([
  "sap/ui/core/mvc/Controller"
], function(Controller) {
  "use strict";

  return Controller.extend("{{namespace}}.controller.PackPanel.EWMObjects.EWMObject", {

    getParent() {
      return this.getView().getParent().getParent().getParent().getController();
    },

    navTo(target, input) {
      this.getParent().navTo(target, input)
    },

    attachCallBackNavigate(cb) {
      this.getParent().attachCallBackNavigate(cb);
    }

  });
});
