sap.ui.define([
  "./EWMObject"
], function(Controller) {
  "use strict";
  return Controller.extend("{{namespace}}.controller.PackPanel.EWMObjects.HandlingUnit", {

    huident: 0,

    onAfterRendering() {
      this.attachCallBackNavigate(function(e) {
        console.log(e)
      });
    },

    onTilePress(e) {
      let key = e.getSource().data("key");
      console.log("Hier geht es noch nicht weiter");
      //this.navTo(key);
    }

  });
});
