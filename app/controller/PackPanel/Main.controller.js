sap.ui.define([
  "../BaseController",
  "sap/ui/model/json/JSONModel"
], function(Controller, JSONModel) {
  "use strict";
  return Controller.extend("{{namespace}}.controller.PackPanel.Main", {

    IDs: {
      navContainer: "nav-container",
      navContainerHU: "nav-container-hu",
      navContainerStorageBin: "nav-container-storage-bin"
    },

    setNavBackVisible(show) {
      this.getView().getModel("mainModel").setProperty("/navBackVisible", show);
    },

    attachCallBackNavigate(cb) {
      this.getView().byId(this.IDs.navContainerHU).addEventDelegate({
        onBeforeShow: cb
      });
    },

    getMainModel() {
      let mainModel = this.getView().getModel("mainModel");

      return mainModel || (mainModel = new JSONModel({
          addEWMObjectType: "",
          navBackVisible: false,
          title: "add.object",
          titleValue: ""
        }),
        this.getView().setModel(mainModel, "mainModel"),
        mainModel);
    },

    onAfterRendering() {
      let cData = this.getView().data();

      let mainModel = this.getMainModel();
      mainModel.setProperty("/addEWMObjectType", cData.type);

    },

    onCloseMainPress() {
      let parent = this.getView().getParent();
      parent.removeItem(this.getView().getId());
    },

    onAddEWMObject(e) {
      let type = this.getMainModel().getProperty("/addEWMObjectType"),
        input = e.getSource().getValue();

      this.navTo(type, input);
    },

    navTo(target, key) {
      switch (target) {
        case "handlingunit":
          this.navToHU(key);
          break;
        case "storagebin":
          this.navToBin(key);
          break;
      }
    },

    setTitle(target, value) {
      let text = "";

      switch (target) {
        case "handlingunit":
          value = 10000003;
          text = "panel.handling.unit.header";
          this.setNavBackVisible(true);
          break;
        case "storagebin":
          value = 10000003;
          text = "panel.storage.bin.header";
          this.setNavBackVisible(true);
          break;
        default:
          text = "add.object";
          this.setNavBackVisible(false);
          break;
      }

      this.getMainModel().setProperty("/title", text);
      this.getMainModel().setProperty("/titleValue", value);
    },

    onAfterNavigate(e) {
      let target = e.getParameter("to").data().target;

      this.setTitle(target, 'GR-Zone');
    },

    onNavBack() {
      this.getView().byId(this.IDs.navContainer).back();
    },

    navToHU(input) {
      this.getView().byId(this.IDs.navContainer).to(this.getView().byId(this.IDs.navContainerHU), 'show', input);
    },

    navToBin(input) {
      this.getView().byId(this.IDs.navContainer).to(this.getView().byId(this.IDs.navContainerStorageBin), 'show', input);
    },

    formatterAddEWMObjectText(type) {
      if (type === "handlingunit") {
        return this.getI18nText("add.ewm.object.hu");
      } else {
        return this.getI18nText("add.ewm.object.storage.bin");
      }
    }

  });
});
