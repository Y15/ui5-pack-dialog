sap.ui.define([
    "./BaseController",
    "sap/ui/model/json/JSONModel",
    "sap/m/FlexItemData",
    "sap/ui/core/CustomData",
    "sap/ui/core/mvc/XMLView"
], function (Controller, JSONModel, FlexItemData, CustomData, XMLView) {
    "use strict";
    return Controller.extend("{{namespace}}.controller.App", {

        IDs: {
            sideNavigation: "sideNavigation",
            mainContainer: "mainContainer"
        },

        onInit: function () {
            jQuery.sap.require("sap.ui.core.util.MockServer");
        },

        onAfterRendering: function () {
            this.initMockUpServer();
        },

        initMockUpServer: function () {
            // Create mockserver
            let oMockServer = new sap.ui.core.util.MockServer({
                rootUri: "http://mymockserver/"
            });

            oMockServer.simulate("model/metadata.xml", "model/");
            oMockServer.start();

            // setting up model
            let oModel = new sap.ui.model.odata.ODataModel("http://mymockserver/", true);
            this.getView().setModel(oModel);
        },

        //Adds Panel for Selection - HU/Storagebin etc.
        addMainPanel: function (type) {
            let mainContainer = this.getView().byId(this.IDs.mainContainer);

            let mainView = new XMLView({
                viewName: "app.view.PackPanel.Main",
                layoutData: new FlexItemData({growFactor: 1}),
                customData: new CustomData({
                    key: "type",
                    value: type
                })
            });

            mainContainer.addItem(mainView);
        },

        //Collapse sidebar Button Pressed
        onCollapseExpandPress: function () {
            let oSideNavigation = this.getView().byId(this.IDs.sideNavigation);
            let bExpanded = oSideNavigation.getExpanded();
            oSideNavigation.setExpanded(!bExpanded);
        },

        //Item in Sidebar Selected
        onSelectItem: function (item) {
            let type = item.getSource().data().type;

            this.logByLocalization("log.add.core.unit", type);
            this.addMainPanel(type);
        }

    });
});