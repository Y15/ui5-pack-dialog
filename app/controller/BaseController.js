sap.ui.define([
    "sap/ui/core/mvc/Controller"
], function (Controller) {
    "use strict";

    return Controller.extend("{{namespace}}.controller.BaseController", {

        getI18nText: function (sKey, aParam) {
            let oBundle = this.getView().getModel("i18n").getResourceBundle();
            return oBundle.getText(sKey, aParam);
        },

        logMessage: function (message) {
            jQuery.sap.log.info(message, "Event", "SALT");
        },

        logByLocalization: function (sKey, aParam) {
            let text = this.getI18nText(sKey, aParam);
            this.logMessage(text);
        },

    });
});